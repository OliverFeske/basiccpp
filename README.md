## First Look into C++

I followed a short tutorial series by **The Morpheus Tutorials** about programming in **C++** and what especially pay attention to.
It went along well and although i still have some issues with converting from C# to C++, it´s pretty similar in case of writing it.

Here are some pictures to show what especially was new to me.

![Header Files](https://imgur.com/AewI6WK.png)*Header Files*

![Operator overloading](https://imgur.com/slPKA8c.png)
![Operator overloading](https://imgur.com/wpgLHJq.png)*Operator Overloading*

![Pointer to Objects](https://imgur.com/3caSZX5.png)*Pointer To Objects*

![Friendship and Inheritance](https://imgur.com/G4Tpmll.png)*Friendship And Inheritance*