#ifndef VECTOR_H
#define VECTOR_H

class Vector {
public:
	static int dimension;											// is the same for every instance of the class, when change in one instance it changes in all instances
	double x, y, z;
	Vector(double a, double b, double c);
	Vector operator +(Vector& b);
	void PrintMe();
};
#endif // !VECTOR_H