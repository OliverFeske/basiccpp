#include "Vector.h"
#include <iostream>

using namespace std;

Vector::Vector(double a, double b, double c) :x(a), y(b), z(c) {}
Vector Vector::operator +(Vector& b)									// thischanges the usage of the plus sign
{
	return Vector(this->x + b.x, this->y + b.y, this->z + b.z);
}
void Vector::PrintMe()											// const after the method name means, that you are not allowed to write, only read (const on front returns aconst value)
{
	cout << "<" << this->x << ", " << this->y << ", " << this->z << ">";				// this is not allowed to be const
}